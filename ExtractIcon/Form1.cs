﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace ExtractIcon
{
	public partial class Form1 : Form
	{
		public const string directory = @"C:\Temp\Test Folder";
		public Form1()
		{
			InitializeComponent();

			var directoryInfo = new DirectoryInfo(directory);

			var files = directoryInfo.GetFiles();
			
			// Set these first, so that when the list populates, it immediately knows which
			// its "Value" and "Display" members are.
			listBox1.DisplayMember = "name";
			listBox1.ValueMember = "fullName";

			listBox1.DataSource = files.Select(f => new
			{
				name = f.Name, 
				fullName = f.FullName
			}).ToList();
		}

		private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
		{
			var icon = Icon.ExtractAssociatedIcon(listBox1.SelectedValue.ToString());

			if (icon == null)
				// Return default image if you're helpful, or just bail out
				return;

			pictureBox1.Image = icon.ToBitmap();
		}
	}
}
